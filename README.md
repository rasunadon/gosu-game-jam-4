# Jelly with Attractive Momentum
- game jam: https://itch.io/jam/gosu-game-jam-4
- gem: (TODO link to RubyGems)
- repository: https://gitlab.com/rasunadon/jelly-with-attractive-momentum
- bug tracker: https://gitlab.com/rasunadon/jelly-with-attractive-momentum/issues
- licence: CC-BY-SA 3.0, Detros (see below for attributions)
- email: rasunadon@seznam.cz

_Jelly with Attractive Momentum_ was created during the fourth Gosu Game Jam
which run for one week between 2023-04-23 and 2023-04-30. Your goal is to
attract the jelly through the rooms with your cursor while either keeping or
braking the momentum so that you can safely accomplish all the needed jumps.

As portals between rooms don't work yet feel free to skip to the next room once
you make jelly to touch the portal. There are three rooms total, so far without
some end screen.

# Controls
- movement: mouse
- show debug info: Tab
- skip to the next room: N
- start a new game: Backspace
- quit: Esc

Backspace abandons the current run and restarts the whole game back to room 0.
To reset the current room simply make jelly leave the window. If you get stuck
in some room, you can also use N to switch to the next one.

# Other documentation
- CHANGELOG: list of what has been added/changed/fixed/removed in given version

# Attributions
- DeltaBreaker, https://opengameart.org/users/deltabreaker
    - on_my_way.wav, CC-BY-SA 3.0, https://opengameart.org/content/on-my-way-8-bit-loop
- crazyduckgames, https://opengameart.org/users/crazyduckgames
    - coin01.wav, CC0, https://opengameart.org/content/soundpack-02
    - jump01.wav, CC0, https://opengameart.org/content/soundpack-02
