TILESIZE = 50
MAPX = 10
MAPY = MAPX
LINE_HEIGHT = 20

WINDOW_WIDTH = MAPX * TILESIZE
WINDOW_HEIGHT = MAPY * TILESIZE
CENTRE = MAPX * TILESIZE / 2

require_relative './background'
require_relative './cursor'
#require_relative './infopane'
require_relative './room'

require_relative './../items/jelly'

ZBACKGROUND = 0
ZITEMS = 1
ZJELLY = 2
ZCURSOR = 3
ZTEXT = 4

# Main window
class GameWindow < Gosu::Window
  attr_reader :win

  def initialize(version,
                 width = WINDOW_WIDTH, \
                 height = WINDOW_HEIGHT, \
                 fullscreen = false)
    super(width, height, fullscreen)

    # Set version name
    self.caption = "Jelly with Attractive Momentum #{version}"
    $debug = false # debug messages turned off by default

    @background = Background.new()
    @cursor = Cursor.new(self)
#    @infopane = Infopane.new(self)
    @room = Room.new(self)
    @jelly = Jelly.new(self, @cursor, @room)

    dir_path = File.dirname(__FILE__)
    @music = Gosu::Song.new(dir_path + '/../../media/on_my_way.wav') # TODO rename files to local names

    restart_game!
  end

  # Start processing the pushed button
  def button_up(key)
    case key
    when Gosu::KB_ESCAPE then
      @music.stop if @music.playing? # just to be nice
      self.close
    when Gosu::KB_BACKSPACE then
      restart_game!
    when Gosu::KB_TAB then
      switch_debug!
    else
      @button = key
    end
  end

  # Process given button
  def update
#    @infopane.update

    unless @won
      @room.update(@button)
      @cursor.update()
      @jelly.update(@button)
      check_win
    end

    # Stop processing this button
    @button = nil
  end

  # Check whether player has won yet
  def check_win
    false # TODO
  end

  # Draw scene
  def draw
    @background.draw
    @cursor.draw
#    @infopane.draw
    @room.draw
    @jelly.draw
  end

  # Load default setup of the whole game
  def restart_game!
    puts "Starting a new game"
    @won = false
    @room.current = 0

    @music.stop if @music.playing?
    @music.volume = 0.4
    @music.play(true) # true = looping

    # Now just reset the scene
    reset!
  end

  # Reset scene
  def reset!
#    @infopane.reset!
    @room.reset!
    @jelly.reset!
  end

  # Switch debug flag
  def switch_debug!
    $debug = !$debug
  end
end
