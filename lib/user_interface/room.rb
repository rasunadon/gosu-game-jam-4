require_relative './../items/block'
require_relative './../items/portal'
require_relative './../items/square'

# types of blocks
BLOCK_PO = 'portal'
BLOCK_SQ = 'square'

# list of blocks: type of block, xx, yy, angle
ROOMS = [{
  id: 0,
  start: [CENTRE, CENTRE],
  blocks: [
    # bottoms
    [BLOCK_SQ, 50, 500, 0],  [BLOCK_SQ, 100, 500, 0], [BLOCK_SQ, 150, 500, 0],
    [BLOCK_SQ, 200, 500, 0], [BLOCK_SQ, 250, 500, 0], [BLOCK_SQ, 300, 500, 0],
    [BLOCK_SQ, 350, 500, 0], [BLOCK_SQ, 400, 500, 0], [BLOCK_SQ, 450, 500, 0],

    # middles
    [BLOCK_SQ, 100, 350, 0],

    # uppers
    [BLOCK_SQ, 150, 200, 0], [BLOCK_SQ, 200, 200, 0], [BLOCK_SQ, 400, 200, 0],
    [BLOCK_SQ, 450, 200, 0], [BLOCK_SQ, 500, 200, 0],

    # portal
    [BLOCK_PO, 475, 150, 325]
  ]
}, {
  id: 1,
  start: [50, 150],
  blocks: [
    # bottoms
    [BLOCK_SQ, 150, 450, 0],  [BLOCK_SQ, 200, 450, 0], [BLOCK_SQ, 250, 450, 0],
    [BLOCK_SQ, 300, 450, 0],  [BLOCK_SQ, 350, 450, 0],

    # middles
    [BLOCK_SQ, CENTRE, CENTRE, 0],

    # uppers
    [BLOCK_SQ, 50, 200, 0],  [BLOCK_SQ, 100, 200, 0],
    [BLOCK_SQ, 400, 200, 0], [BLOCK_SQ, 450, 200, 0],

    # portal
    [BLOCK_PO, 475, 150, 325]
  ]
},{
  id: 2,
  start: [50, 50],
  blocks: [
    # bottoms
    [BLOCK_SQ, 50, 500, 0],  [BLOCK_SQ, 100, 500, 0], [BLOCK_SQ, 150, 500, 0],

    # middles
    [BLOCK_SQ, 400, 400, 0], [BLOCK_SQ, 450, 400, 0], [BLOCK_SQ, 500, 400, 0],

    # uppers
    [BLOCK_SQ, 50, 100, 0],  [BLOCK_SQ, 100, 100, 0], [BLOCK_SQ, 450, 200, 0],

    # portal
    [BLOCK_PO, 475, 150, 325],
  ]
},]


class Room
  attr_reader :blocks, :start_x, :start_y
  attr_writer :current

  def initialize(window)
    dir_path = File.dirname(__FILE__)
    @portal_sound = Gosu::Sample.new(dir_path + '/../../media/coin01.wav')

    @window = window
    @current = 0

    reset!
  end

  # Load default setup of the current room
  def reset!
    puts "Loading room #{@current}"
    rr = ROOMS.select {|ii| ii[:id] == @current}[0]

    # Set starting position
    @start_x = rr[:start][0]
    @start_y = rr[:start][1]

    # Load all blocks
    @blocks = []
    rr[:blocks].each {|bb|
      case bb[0] # type of block
      when BLOCK_SQ
        @blocks.append(Square.new(bb[1], bb[2], bb[3]))
      when BLOCK_PO
        @blocks.append(Portal.new(bb[1], bb[2], bb[3]))
      end
    }
  end

  def update(button)
    case button
      when Gosu::KB_N then # cheat (and for now the only way)
        next_room!
    end

    @blocks.each {|bb| bb.update}
  end

  def next_room!
    @portal_sound.play(0.5, 0.5, false) # volume, speed, looping
    @current = (@current + 1).modulo(ROOMS.size)
    @window.reset!
  end

  def draw
    @blocks.each {|bb| bb.draw}
  end
end
