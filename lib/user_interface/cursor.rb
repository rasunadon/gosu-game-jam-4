CURSOR_SIZE = 0.25

class Cursor # TODO Magnet?
  attr_reader :x, :y

  def initialize(window)
    dir_path = File.dirname(__FILE__)
    @image = Gosu::Image.new(dir_path + '/../../media/circle.png')

    @window = window
    @x = CENTRE
    @y = CENTRE
  end

  def update()
    @x = @window.mouse_x.to_int
    @y = @window.mouse_y.to_int
  end

  def draw
    @image.draw_rot(@x, @y, ZCURSOR, 0,
                    center_x = 0.5, center_y = 0.5,
                    scale_x = CURSOR_SIZE, scale_y = CURSOR_SIZE,
                    color = Gosu::Color::RED)

    if $debug
      coords = Gosu::Image.from_text("[#{@x}, #{@y}]", LINE_HEIGHT)
      coords.draw_rot(@x, @y, ZTEXT, 0)
    end
  end
end
