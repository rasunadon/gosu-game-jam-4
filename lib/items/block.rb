BLOCK_XSCALE = 1
BLOCK_YSCALE = 1

class Block
  attr_accessor :x, :y

  def initialize(xx, yy, angle)
    dir_path = File.dirname(__FILE__)
    @image = Gosu::Image.new(dir_path + '/../../media/square.png')

    @x = xx
    @y = yy
    @angle = angle
  end

  def update()
  end

  # DEBUG only
  def look_below
    puts left
    puts right
    puts top
    puts bottom
  end

  def draw
    @image.draw_rot(@x, @y, ZITEMS, @angle,
                    center_x = 0.5, center_y = 0.5,
                    scale_x = @scale_x, scale_y = @scale_y,
                    color = @colour)

    if $debug
      coords = Gosu::Image.from_text("[#{@x}, #{@y}, #{@angle}°]", LINE_HEIGHT)
      coords.draw_rot(@x, @y, ZTEXT, @angle)
    end
  end

  def width
    BLOCK_XSCALE * TILESIZE
  end

  def height
    BLOCK_YSCALE * TILESIZE
  end

  # From (lower) x-border
  def left
    @x - width / 2
  end

  # To (bigger) x-border
  def right
    @x + width / 2
  end

  # From (lower) y-border
  def top
    @y - height / 2
  end

  # To (bigger) y-border
  def bottom
    @y + height / 2
  end
end
