SPEED_PRECISION = 3 # rounding precision of speed (decimal places)
JELLY_SIDE_SPEED = 4 # max x-axis speed # TODO convert to upgradable variable
JELLY_JUMP_SPEED = 2 * JELLY_SIDE_SPEED # y-axis change in speed when jumping
JELLY_SIDE_ACCEL = 0.1 # x-axis acceleration per tick
JELLY_FALL_ACCEL = JELLY_JUMP_SPEED / 2.0 # y-axis acceleration per tick

STATUS_STANDING = 0
STATUS_INAIR = 1

class Jelly < Block
  def initialize(window, cursor, room)
    dir_path = File.dirname(__FILE__)
    @image = Gosu::Image.new(dir_path + '/../../media/square.png')
    @jump_sound = Gosu::Sample.new(dir_path + '/../../media/jump01.wav')

    @window = window
    @cursor = cursor
    @room = room

    reset!
  end

  # Load default setup of current room
  def reset!
    @x = @room.start_x
    @y = @room.start_y
    @angle = 0

    @speed_x = 0
    @speed_y = 0

    # TODO sound: reset
    start_falling!
  end

  def update(button)
    case button
      when Gosu::MS_LEFT then
        unless @status == STATUS_INAIR
          jump!
        else
          puts 'Can\'t jump in midair'
#          puts @speed_y # DEBUG\
        end
      when Gosu::MS_RIGHT then
        jump! # DEBUG
    end

    move
  end

  def jump!
    @jump_sound.play(0.6, 0.6, false) # volume, speed, looping
    start_falling! # (re)start falling
    @speed_y -= JELLY_JUMP_SPEED
  end

  def start_falling!
    @status = STATUS_INAIR
    @speed_y = 0 # start anew
    @time_of_start_of_fall = Gosu.milliseconds
  end

  def stop_falling!
    @status = STATUS_STANDING # TODO bounce first
    @speed_y = 0 # stop
    @time_of_start_of_fall = nil
  end

  # Follow mouse but curtail your movement (by collisions with blocks)
  def move
    accelerate!
    limit_speed!
    move!
    check_collisions
    check_bounds
  end

  # Update acceleration
  def accelerate!
    # X=axis
    if (@x - @cursor.x) > 0
      @speed_x -= JELLY_SIDE_ACCEL
    end

    if (@x - @cursor.x) < 0
      @speed_x += JELLY_SIDE_ACCEL
    end

    # Y-axis
    if @status == STATUS_INAIR
      seconds_falling = (Gosu.milliseconds - @time_of_start_of_fall) / 1000.0
      @speed_y += JELLY_FALL_ACCEL * seconds_falling * seconds_falling
    end
  end

  # Limit speed to +/- max speed
  def limit_speed!
    @speed_x = @speed_x.round(SPEED_PRECISION)
    @speed_y = @speed_y.round(SPEED_PRECISION)

    @speed_x = [[-1 * JELLY_SIDE_SPEED, @speed_x].max, JELLY_SIDE_SPEED].min
    @speed_y = [[-0.9 * TILESIZE, @speed_y].max, 0.9 * TILESIZE].min # no quantum tunneling
  end

  # Apply speed
  def move!
    @x += @speed_x
    @y += @speed_y
  end

  # Deal with collisions, by falling or moving aside
  def check_collisions
    collisions = collect_collisions

    # Falling off ledges
    ledges = collisions.select {|ii| ii[4]} # bottom collision
    if @status == STATUS_STANDING && ledges.empty?
      start_falling!
    end

    # Moving aside
    unless collisions.empty?
      # TODO prioritize collisions on bottom?
      # TODO then use closest block, not just the first?
      revert_collision!(collisions[0])
    end
  end

  # Find collisions and note their directions
  def collect_collisions
    ret = []

    @room.blocks.each {|bb|
      coll_left = bb.left < self.left && self.left < bb.right
      coll_right = bb.left < self.right && self.right < bb.right
      coll_top = bb.top < self.top && self.top < bb.bottom
      coll_bottom = bb.top <= self.bottom && self.bottom < bb.bottom

      # Store only collided blocks
      if (coll_left || coll_right) && (coll_top || coll_bottom)
        # TODO store collision impact instead (distance in given direction)
        # so that the biggest collision can be dealt with
        ret.append([bb, coll_left, coll_right, coll_top, coll_bottom])
      end
    }

    return ret
  end

  def revert_collision!(collided)
    # Down before Up/Left/Right, TODO better prioritization
    # Collision on the bottom, move up
    if collided[4]
      @y = collided[0].top - height / 2

      if @status = STATUS_INAIR
        stop_falling!
      end
      return
    end

    # Collision on the top, move down
    if collided[3]
      @y = collided[0].bottom + height / 2
      # no stop_falling! here so that ceiling-jumps are possible
      return
    end

    # Collision on the left, move right
    if collided[1]
      @x = collided[0].right + width / 2
      return
    end

    # Collision on the right, move left
    if collided[2]
      @x = collided[0].left - width / 2
      return
    end
  end

  # Reset if out of bounds of the window
  def check_bounds
    if right < 0 || left > WINDOW_WIDTH || bottom < 0 || top > WINDOW_HEIGHT
      puts 'Out of bounds, resetting'
      reset!
    end
  end

  def draw
    @image.draw_rot(@x, @y, ZJELLY, 0,
                    center_x = 0.5, center_y = 0.5,
                    scale_x = BLOCK_XSCALE, scale_y = BLOCK_YSCALE,
                    color = Gosu::Color::GREEN)

    if $debug
      coords = Gosu::Image.from_text("[#{@x}, #{@y}, #{@angle}°]", LINE_HEIGHT)
      coords.draw_rot(@x, @y, ZTEXT, @angle)
    end
  end

  # TODO check_portal
  # use coin01.wav while being sucked into it
end
