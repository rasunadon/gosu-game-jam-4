class Square < Block
  def initialize(xx, yy, angle)
    super

    @scale_x = BLOCK_XSCALE
    @scale_y = BLOCK_YSCALE
    @colour = Gosu::Color::GRAY
  end
end
