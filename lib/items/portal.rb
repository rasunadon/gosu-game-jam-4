class Portal < Block
  def initialize(xx, yy, angle)
    super

    @scale_x = BLOCK_XSCALE / 2.0
    @scale_y = BLOCK_YSCALE / 2.0
    @colour = Gosu::Color::CYAN
  end

  def update
    @angle = (@angle - rand(10) ^ 3).modulo(360)
  end
end
