# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## Not released yet
### Added
### Changed
### Fixed
### Removed


## [v5](../../tags/v5) – 2023-04-30
### Added
- music
- portals (not yet working)
- jump and portal sounds

### Changed
- square as a separate class


## [v4](../../tags/v4) – 2023-04-30
### Added
- two more rooms
- summary in README
- cheat of skipping to the next room
- marketing media

### Changed
- collision detection slightly relaxed
- reset of scene separated from starting of a new game


## [v3](../../tags/v3) – 2023-04-29
### Added
- jumping
- falls affected by gravity
- falling off ledges
- death by being outside of the bounds of the window

### Changed
- starting position parametrized

### Fixed
- fall speed capped under tilesize per tick to disallow quantum tunneling


## [v2](../../tags/v2) – 2023-04-26
### Added
- collision detection
- acceleration of movement
- visible cursor

### Changed
- directional movement instead of teleporting


## [v1](../../tags/v1) – 2023-04-25
### Added
- close, reset and debug hotkeys
- basic scene with few platforms
- jelly follows mouse
